from qa_automation.Vittaly_Murovei_D.tools.page_object.main_pages import \
    MainPaige


def test_regional_setting_title(driver):
    """
    Тест проверки title.
    :param driver: драйвер.
    """
    setting_page = MainPaige(driver)
    setting_page.open()
    setting_page.get_region_setting_page()
    assert setting_page.get_title() == "Regional Settings | My Store"


def test_choice_currency(driver):
    """
    Тест проверки выбранной валюты.
    :param driver: драйвер.
    """
    currency_space = MainPaige(driver)
    currency_space.open()
    currency = currency_space.get_region_setting_page()
    assert currency.choice_currency().text == 'EUR'


def test_choice_country(driver):
    """
    Тест проверки выбранной страны.
    :param driver: драйвер.
    """
    currency_space = MainPaige(driver)
    currency_space.open()
    country_1 = currency_space.get_region_setting_page()
    assert country_1.choice_country().text == 'Belarus'
