from qa_automation.Vittaly_Murovei_D.tools.page_object.main_pages import \
    MainPaige


def test_main_page_title(driver):
    """
    Проверка title.
    :param driver: драйвер.
    :return:элемент.
    """
    main_page = MainPaige(driver)
    main_page.open()
    assert main_page.get_title() == "Online Store | My Store"


def test_check_main_page_img(driver):
    """
    Проверка картинки главной страницы.
    :param driver: драйвер.
    :return: элемент.
    """
    main_img = MainPaige(driver)
    main_img.open()
    assert main_img.check_main_page_img()


def test_find_login_form(driver):
    """
    Тест наличия формы логирования на странице
    :param driver: драйвер
    :return: элемент.
    """
    login_form = MainPaige(driver)
    login_form.open()
    assert login_form.login_form()
