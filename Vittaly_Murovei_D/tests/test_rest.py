from qa_automation.Vittaly_Murovei_D.tools.rest_api.put_get_delete \
    import PostDelPet


def test_post_pet(driver):
    """
    Тест REST запросов.
    :param driver: драйвер.
    """
    test = PostDelPet(driver)
    assert test.add_pet() == 200
    assert test.get_pet() == 200
    assert test.del_pet() == 200
    assert test.get_pet() == 404
