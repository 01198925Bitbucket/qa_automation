from qa_automation.Vittaly_Murovei_D.tools.page_object.main_pages import \
    MainPaige


def test_filling_form_login(driver):
    """
    Тест успешного логирования.
    :param driver: драйвер.
    :return: элемент, подтверждающий успешное логирование.
    """
    filling_login = MainPaige(driver)
    filling_login.open()
    login_in = filling_login.login_form()
    assert login_in.filling_form_login()


def test_order(driver):
    """
    Тест заказа.
    :param driver:  драйвер.
    """
    filling_login = MainPaige(driver)
    filling_login.open()
    login_in = filling_login.login_form()
    login_in.filling_form_login()
    order = login_in.order_red_duck()
    assert order.text == '$60.00'


def test_check_get_order(driver):
    """
    Тест проверки того, что заказ сделан.
    :param driver: драйвер.
    """
    filling_login = MainPaige(driver)
    filling_login.open()
    login_in = filling_login.login_form()
    login_in.filling_form_login()
    login_in.order_red_duck()
    order = login_in.get_order()
    assert order.text == 'Your order is successfully completed!'


def test_db(driver):
    """
    Тест проверки заказа в базе данных.
    :param driver: драйвер.
    """
    filling_login = MainPaige(driver)
    filling_login.open()
    login_in = filling_login.login_form()
    login_in.filling_form_login()
    login_in.order_red_duck()
    login_in.get_order()
    check = login_in.connect()
    assert check == 60
