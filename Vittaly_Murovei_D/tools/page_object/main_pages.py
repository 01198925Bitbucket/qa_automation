from qa_automation.Vittaly_Murovei_D.tools.page_object.base_pages import \
    BasePage
from qa_automation.Vittaly_Murovei_D.tools.page_object.login_order_check \
    import LoginFormIn
from qa_automation.Vittaly_Murovei_D.tools.page_object.regional_setting_page \
    import RegionalSetting
from qa_automation.Vittaly_Murovei_D.tools.page_locators.main_locators import \
    MainPageLocators


class MainPaige(BasePage):
    """
    Класс главной страницы.
    """
    URL = "http://localhost/litecart/en/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_title(self):
        """
        Метод получения заголовка страницы.
        :return: заголовок.
        """
        return self.driver.title

    def check_main_page_img(self):
        """
        Проверка наличия картинки главной страницы.
        :return:элемент.
        """
        return self.find_element(MainPageLocators.LOCATOR_MAIN_PAGE_IMG)

    def login_form(self):
        """
        Поиск формы логирования на страницы.
        :return: елемент.
        """
        return LoginFormIn(self.driver, self.driver.current_url)

    def get_region_setting_page(self):
        """
        Метод открытия региональных настроек
        :return: страница настроек.
        """
        setting_link = self.find_element(MainPageLocators.
                                         LOCATOR_REGIONAL_SETTING)
        setting_link.click()
        return RegionalSetting(self.driver, self.driver.current_url)
