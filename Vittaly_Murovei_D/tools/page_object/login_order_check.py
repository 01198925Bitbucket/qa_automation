import MySQLdb
import MySQLdb.cursors

from qa_automation.Vittaly_Murovei_D.tools.page_locators.ducks_order_locator \
    import DuckOrder
from qa_automation.Vittaly_Murovei_D.tools.page_object.base_pages \
    import BasePage
from qa_automation.Vittaly_Murovei_D.tools.page_locators.login_form_locators \
    import LoginFormLocators


class LoginFormIn(BasePage):
    """
    Класс формы логирования.
    """

    def filling_form_login(self):
        """
        Метод заполнения формы логирования.
        :return: елемент.
        """
        filling_email = self.find_element(LoginFormLocators.
                                          INPUT_EMAIL_LOCATOR)
        filling_email.send_keys('vitali.muravei@sanofi.com')
        filling_password = self.find_element(LoginFormLocators.
                                             INPUT_PASSWORD_LOCATOR)
        filling_password.send_keys('admin')
        button_login = self.find_element(LoginFormLocators.
                                         BUTTON_LOGIN_LOCATOR)
        button_login.click()
        success_login = self.find_element(LoginFormLocators.
                                          LOGIN_SUCCESS_LOCATOR)
        return success_login

    def order_red_duck(self):
        """
        Заказ уток и проверка суммы.
        :return: элемент с суммой заказа.
        """
        if self.find_element(DuckOrder.ITEM_IN_CART).text == "0":

            duck_buy = self.find_element(DuckOrder.RED_DUCK_LOCATOR)
            duck_buy.click()
            duck_buy = self.find_element(DuckOrder.BUTTON_ADD_TO_CART)
            duck_buy.click()
            duck_buy = self.find_element(DuckOrder.BUTTON_ADD_TO_CART)
            duck_buy.click()
            duck_buy = self.find_element(DuckOrder.BUTTON_ADD_TO_CART)
            duck_buy.click()
            cart = self.find_element(DuckOrder.BUTTON_CHECKOUT)
            cart.click()
            total = self.find_element(DuckOrder.CHECK_SUMM_LOCATOR)
            return total
        else:
            cart = self.find_element(DuckOrder.BUTTON_CHECKOUT)
            cart.click()
            remove = self.find_element(DuckOrder.BUTTON_REMOVE)
            remove.click()
            back = self.find_element(DuckOrder.BACK_BUTTON)
            back.click()
            duck_buy = self.find_element(DuckOrder.RED_DUCK_LOCATOR)
            duck_buy.click()
            duck_buy = self.find_element(DuckOrder.BUTTON_ADD_TO_CART)
            duck_buy.click()
            duck_buy = self.find_element(DuckOrder.BUTTON_ADD_TO_CART)
            duck_buy.click()
            duck_buy = self.find_element(DuckOrder.BUTTON_ADD_TO_CART)
            duck_buy.click()
            cart = self.find_element(DuckOrder.BUTTON_CHECKOUT)
            cart.click()
            total = self.find_element(DuckOrder.CHECK_SUMM_LOCATOR)
            return total

    def get_order(self):
        """
        Функция совершения поупки
        :return:
        """
        order_get = self.find_element(DuckOrder.CONFIRM_ORDER)
        order_get.click()
        check_confirm = self.find_element(DuckOrder.CHECK_CONFIRM_ORDER)
        return check_confirm

    @staticmethod
    def connect():
        db = MySQLdb.connect(
            user="root",
            password="",
            host="127.0.0.1",
            database="litecart"
        )
        cursor = db.cursor()
        query = b"""SELECT payment_due
                FROM
                lc_orders WHERE 1"""
        cursor.execute(query)
        for row in cursor.fetchall():
            for a in row:
                a_str = str(a)
                a_float = float(a_str)
                a_int = int(a_float)
                return a_int
