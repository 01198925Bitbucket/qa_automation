from qa_automation.Vittaly_Murovei_D.tools.page_object.base_pages \
    import BasePage
from qa_automation.Vittaly_Murovei_D.tools.page_locators. \
    regional_setting_locators import RegionalSettingLocators
from selenium.webdriver.support.ui import Select


class RegionalSetting(BasePage):
    """
    Класс страницы региональных настроек.
    """

    def get_title(self):
        """
        Метод получения загаловка страницы.
        :return: заголовок.
        """
        return self.driver.title

    def choice_currency(self):
        """
        Метод изменения валюты.
        :return: элемент.
        """
        choice_currency = self.find_element(RegionalSettingLocators.
                                            LOCATOR_CURRENCY)
        Select(choice_currency).select_by_value("EUR")
        button_save = self.find_element(RegionalSettingLocators.
                                        LOCATOR_BUTTON_SAVE)
        button_save.click()
        currency = self.find_element(RegionalSettingLocators.
                                     LOCATOR_CHECK_CURRENCY)
        return currency

    def choice_country(self):
        """
        Метод изменения страны.
        :return: элемент.
        """
        choice_country = self.find_element(RegionalSettingLocators.
                                           LOCATOR_COUNTRY)
        Select(choice_country).select_by_value("BY")
        button_save = self.find_element(RegionalSettingLocators.
                                        LOCATOR_BUTTON_SAVE)
        button_save.click()
        country = self.find_element(RegionalSettingLocators.
                                    LOCATOR_CHECK_COUNTRY)
        return country
