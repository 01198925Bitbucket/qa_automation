import requests

from qa_automation.Vittaly_Murovei_D.tools.page_object.base_pages import \
    BasePage


class PostDelPet(BasePage):
    URL = "https://petstore.swagger.io/v2/pet"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def add_pet(self):
        """
        Метод добавления питомца.
        :return: код.
        """
        response = requests.post('https://petstore.swagger.io/v2/pet',
                                 json={
                                     "id": 46,
                                     "category": {
                                         "id": 46,
                                         "name": "Dog"
                                     },
                                     "name": "Dog",
                                     "photoUrls": [
                                         "string"
                                     ],
                                     "tags": [
                                         {
                                             "id": 46,
                                             "name": "Dog"
                                         }
                                     ],
                                     "status": "available"
                                 })
        return response.status_code

    def get_pet(self):
        """
        Метод проверки добавленного питомца.
        :return: код.
        """
        link = 'https://petstore.swagger.io/v2/pet'
        response = requests.get(f"{link}/{46}")
        return response.status_code

    def del_pet(self):
        """
        Метод удаления питомца по id.
        :return: код.
        """
        link = 'https://petstore.swagger.io/v2/pet'
        response = requests.delete(f"{link}/{46}")
        return response.status_code

    def get_pet_after_del(self):
        """
        Метод проверки питомца после удаления.
        :return: код (нужен 404).
        """
        link = 'https://petstore.swagger.io/v2/pet'
        response = requests.get(f"{link}/{46}")
        return response.status_code
