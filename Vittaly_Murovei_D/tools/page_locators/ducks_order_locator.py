from selenium.webdriver.common.by import By


class DuckOrder:
    """
    Локаторы для заказа уток.
    """
    RED_DUCK_LOCATOR = (By.XPATH, "//img[@src='/litecart/cache/4b39bc247966d"
                                  "79d12deb0fbf1ab731b6264f037160x160_fwb."
                                  "png']")
    BUTTON_ADD_TO_CART = (By.XPATH, "//button[@type='submit']")
    BUTTON_CHECKOUT = (By.XPATH, "//a[contains(text(), 'Checkout')]")
    CHECK_SUMM_LOCATOR = (By.XPATH, "//tr[@class='footer']/td[2]")
    BUTTON_REMOVE = (By.XPATH, "//button[@value='Remove']")
    ITEM_IN_CART = (By.XPATH, "//span[@class='quantity']")
    BACK_BUTTON = (By.XPATH, "//a[contains(text(), '<< Back')]")
    CONFIRM_ORDER = (By.XPATH, "//button[@type='submit' and "
                               "@name='confirm_order']")
    CHECK_CONFIRM_ORDER = (By.XPATH, "//h1[text()='Your order is successfully "
                                     "completed!']")
