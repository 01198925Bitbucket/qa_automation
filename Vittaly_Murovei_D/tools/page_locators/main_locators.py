from selenium.webdriver.common.by import By


class MainPageLocators:
    """
    Класс с локаторами главной страницы.
    """
    LOGIN_FORM_LOCATOR = (By.XPATH, "//div[@id='box-account-login']")
    LOCATOR_REGIONAL_SETTING = (By.PARTIAL_LINK_TEXT, "Regional Settings")
    LOCATOR_MAIN_PAGE_IMG = (By.XPATH, "//img[@src='/litecart/images/slides"
                                       "/1-lonely-duck.jpg']")

