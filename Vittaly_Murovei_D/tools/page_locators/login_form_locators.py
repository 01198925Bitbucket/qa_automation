from selenium.webdriver.common.by import By


class LoginFormLocators:
    """
    Локаторы формы логирования.
    """
    INPUT_EMAIL_LOCATOR = (By.XPATH, "//input[@type='text']")
    INPUT_PASSWORD_LOCATOR = (By.XPATH, "//input[@type='password']")
    BUTTON_LOGIN_LOCATOR = (By.XPATH, "//button[@value='Login']")
    LOGIN_SUCCESS_LOCATOR = (By.XPATH, "//a[@href='http://localhost/"
                                       "litecart/en/logout']")
