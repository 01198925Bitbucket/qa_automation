from selenium.webdriver.common.by import By


class RegionalSettingLocators:
    """
    Локаторы страницы региональных настроек
    """
    LOCATOR_CURRENCY = (By.XPATH, "//select[@name='currency_code']")
    LOCATOR_COUNTRY = (By.XPATH, "//select[@name='country_code']")
    LOCATOR_CHECK_CURRENCY = (By.XPATH, "//div[@class='currency']")
    LOCATOR_CHECK_COUNTRY = (By.XPATH, "//div[@class='country']")
    LOCATOR_BUTTON_SAVE = (By.XPATH, "//button[@type='submit']")
